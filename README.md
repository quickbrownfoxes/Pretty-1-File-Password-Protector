# Pretty-1-File-Password-Protector
A simple password protector for simple non-critical applications.

**Do not use this for critical applications!! The password is stored in the PHP file, if you have a misconfiguration in your PHP installation or you have a download script anywhere, the password will be exposed!** 

By using this you understand the risks described above, if you're looking for other options that don't require a database I would highly recommend a SSO option with Google, Facebook, etc.

## Usage
1. Add the include('password_protect.php'); to the top the page you want to protect.
<br />

    
    <?php 
    include('password_protect.php');
    "your php code here"
    
2. Change the config, for the username and password, just open the password-protect.php and change this at the top of the file. There are other config options there also!
<br />

    $LOGIN_INFORMATION = array(
      'admin' => 'hunter2'
    );
    
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*
